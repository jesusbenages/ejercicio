<?php
require_once 'entity/ImagenGaleria.php';
require_once 'entity/Asociado.php';
require_once 'utils/utils.php';
require_once 'repository/ImagenGaleriaRepository.php';
require_once 'repository/AsociadoRepository.php';
require_once 'entity/ImagenGaleria.php';
require_once 'entity/Categoria.php';
require_once 'core/App.php';
require_once 'database/Connection.php';


$imgRepository = new ImagenGaleriaRepository();
$imagenes = $imgRepository->findAll();

$asociadoRepository = new AsociadoRepository();
$asociados = $asociadoRepository->findAll();


$asociados = obtenerArrayReducido($asociados,3);
require __DIR__ . '/../views/index.view.php';
